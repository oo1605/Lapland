function b() {
    // h = $(window).height();
    t = $(document).scrollTop();
    if (t > 200) {
        $('#gotop').show().css("display", "block");
    } else {
        $('#gotop').hide();
    }
}

$(document).ready(function (e) {
    $('#gotop').click(function () {
        $(document).scrollTop(0);
    });

    $(window).scroll(function (e) {
        b();
    });
});

