// var DBServiceURI = "http://localhost:63342/Lapland-front/";

window.onload = function () {

    // 我的名片
    loadCard();
    // 图片展示
    loadInfo();
    // 文章列表
    loadArticleList();
    // 热门点击
    loadRecommend();
}

// 我的名片
function loadCard() {
    $.ajax({
        type: "get",
        url: "../../json/card.json",
        async: true,
        success: function (data) {
            var data = data.data;
            var cardName = document.getElementById("cardName");
            var cardWork = document.getElementById("cardWork");
            var cardDesc = document.getElementById("cardDesc");

            cardName.innerHTML = data.name;
            cardWork.innerHTML = data.work;
            cardDesc.innerHTML = data.desc;
        }
    });
}

// 图片展示
function loadInfo() {
    $.ajax({
        type: "get",
        url: "../../json/info.json",
        async: true,
        success: function (data) {
            var data = data.data;
            var infoTitle = document.getElementById("title");
            var infoContent = document.getElementById("content");

            infoTitle.innerHTML = data.title;
            infoContent.innerHTML = data.content;
        }
    });
}

// 文章列表
function loadArticleList() {
    $.ajax({
        type: "get",
        url: "../../json/blog.json",
        async: true,
        success: function (data) {
            var data = data.data;

            var parentDiv = document.getElementsByClassName("bloglist");
            for (var i = 0; i < data.length; i++) {

                var childDiv_1 = $('<li></li>');
                childDiv_1.appendTo(parentDiv[0]);

                var childDiv_2 = $('<div></div>');
                childDiv_2.addClass('arrow_box');
                childDiv_2.appendTo(childDiv_1);

                // 三角形
                var childDiv_3 = $('<div></div>');
                childDiv_3.addClass('ti');
                childDiv_3.appendTo(childDiv_2);
                // 圆形
                var childDiv_4 = $('<div></div>');
                childDiv_4.addClass('ci');
                childDiv_4.appendTo(childDiv_2);
                // 标题
                var childDiv_5 = $('<h2></h2>');
                childDiv_5.addClass('title');
                var childDiv_5_1 = $('<a></a>');
                childDiv_5_1.text(data[i].title);
                childDiv_5_1.attr('href', data[i].url);
                childDiv_5_1.appendTo(childDiv_5);
                childDiv_5.appendTo(childDiv_2);
                // 摘要
                var childDiv_6 = $('<ul></ul>');
                childDiv_6.addClass('abstract');
                childDiv_6.appendTo(childDiv_2);
                var childDiv_6_1 = $('<a></a>');
                childDiv_6_1.attr('href', data[i].url);
                childDiv_6_1.appendTo(childDiv_6);
                var childDiv_6_1_1 = $('<img>');
                childDiv_6_1_1.attr('src', data[i].img_url);
                childDiv_6_1_1.appendTo(childDiv_6_1);
                var childDiv_6_2 = $('<p></p>');
                childDiv_6_2.text(data[i].abstract);
                childDiv_6_2.appendTo(childDiv_6);
                // 文章信息
                var childDiv_7 = $('<ul></ul>');
                childDiv_7.addClass('details');
                childDiv_7.appendTo(childDiv_2);
                // 发布/更新日期
                var childDiv_7_1 = $('<li></li>');
                childDiv_7_1.appendTo(childDiv_7);
                var childDiv_7_1_1 = $('<a></a>');
                childDiv_7_1_1.attr('href', '');
                childDiv_7_1_1.attr('title', '发布日期：' + data[i].details.update_date);
                childDiv_7_1_1.appendTo(childDiv_7_1);
                var childDiv_7_1_1_1 = $('<i></i>');
                childDiv_7_1_1_1.addClass('fa fa-clock-o');
                childDiv_7_1_1_1.appendTo(childDiv_7_1_1);
                $(childDiv_7_1_1_1).after(data[i].details.update_date);
                // 作者
                var childDiv_7_2 = $('<li></li>');
                childDiv_7_2.appendTo(childDiv_7);
                var childDiv_7_2_1 = $('<a></a>');
                childDiv_7_2_1.attr('href', '');
                childDiv_7_2_1.attr('title', '作者：' + data[i].details.author);
                childDiv_7_2_1.appendTo(childDiv_7_2);
                var childDiv_7_2_1_1 = $('<i></i>');
                childDiv_7_2_1_1.addClass('fa fa-user');
                childDiv_7_2_1_1.appendTo(childDiv_7_2_1);
                $(childDiv_7_2_1_1).after(data[i].details.author);
                // 评论数
                var childDiv_7_3 = $('<li></li>');
                childDiv_7_3.appendTo(childDiv_7);
                var childDiv_7_3_1 = $('<a></a>');
                childDiv_7_3_1.attr('href', '');
                childDiv_7_3_1.attr('title', '已有' + data[i].details.comment_num + '条评论');
                childDiv_7_3_1.appendTo(childDiv_7_3);
                var childDiv_7_3_1_1 = $('<i></i>');
                childDiv_7_3_1_1.addClass('fa fa-commenting');
                childDiv_7_3_1_1.appendTo(childDiv_7_3_1);
                $(childDiv_7_3_1_1).after(data[i].details.comment_num);
                // 阅览数
                var childDiv_7_4 = $('<li></li>');
                childDiv_7_4.appendTo(childDiv_7);
                var childDiv_7_4_1 = $('<a></a>');
                childDiv_7_4_1.attr('href', '');
                childDiv_7_4_1.attr('title', '已有' + data[i].details.look_num + '次浏览');
                childDiv_7_4_1.appendTo(childDiv_7_4);
                var childDiv_7_4_1_1 = $('<i></i>');
                childDiv_7_4_1_1.addClass('fa fa-eye');
                childDiv_7_4_1_1.appendTo(childDiv_7_4_1);
                $(childDiv_7_4_1_1).after(data[i].details.look_num);
                // 分类
                var childDiv_7_5 = $('<li></li>');
                childDiv_7_5.appendTo(childDiv_7);
                var childDiv_7_5_1 = $('<a></a>');
                childDiv_7_5_1.attr('href', '');
                childDiv_7_5_1.attr('title', '分类：' + data[i].details.category);
                childDiv_7_5_1.appendTo(childDiv_7_5);
                var childDiv_7_5_1_1 = $('<i></i>');
                childDiv_7_5_1_1.addClass('fa fa-th-list');
                childDiv_7_5_1_1.appendTo(childDiv_7_5_1);
                $(childDiv_7_5_1_1).after(data[i].details.category);
            }
        },
        error: function (data) {
            console.log("数据请求失败！" + data.msg);
        }
    });
}


function loadRecommend() {
    $.ajax({
        type: "get",
        url: "../../json/blog.json",
        async: true,
        success: function (data) {
            var data = data.data;
            var parentDiv = document.getElementsByClassName("recommendList");

            for (var i = 0; i < 9; i++) {
                var childDiv_1 = $('<li></li>');
                var childDiv_1_1 = $('<span></span>');
                var childDiv_1_1_1 = $('<strong></strong>');
                childDiv_1_1_1.text(i+1);
                childDiv_1_1_1.appendTo(childDiv_1_1);
                childDiv_1_1.appendTo(childDiv_1);
                childDiv_1.appendTo(parentDiv[0]);
                var childDiv_1_2 = $('<a></a>');
                childDiv_1_2.attr('href', data[i].url);
                childDiv_1_2.text(data[i].title);
                childDiv_1_2.appendTo(childDiv_1);
            }
        }
    });
}
